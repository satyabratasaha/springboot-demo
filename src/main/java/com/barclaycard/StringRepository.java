package com.barclaycard;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StringRepository extends JpaRepository<CompareObjects, Long> {
String findByLeftString(Long id);
String findByRightString(Long id);
List<CompareObjects> findById(Long id);

}
