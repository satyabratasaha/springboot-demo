package com.barclaycard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/v1/compare")
public class DemoController {
	
	StringRepository stringRepository;
	
	@Autowired
	public DemoController(StringRepository stringRepository) {
		this.stringRepository=stringRepository;
	}
	
	@RequestMapping(value="/hello")
	public String hello(){
		return "Hello World";
	}
	
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public ResponseEntity<CompareObjects> create(@RequestBody CompareObjects compare){
		stringRepository.save(compare);
		return new ResponseEntity<>(HttpStatus.CREATED);		
	}
	
	@RequestMapping(value="/getall",method=RequestMethod.GET)
	public List<CompareObjects> getAll(){
		return stringRepository.findAll();		
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public List<CompareObjects> getById(@PathVariable long id){
		return stringRepository.findById(id);
	}
	
	@RequestMapping(value="/delta/{id}",method=RequestMethod.GET)
	public Boolean getDeltaById(@PathVariable long id){
		String r=stringRepository.findById(id).get(0).getLeftString();
		String l=stringRepository.findById(id).get(0).getRightString();
		Boolean b = r.equals(l);
		return b;
	}
	
	
	

}
